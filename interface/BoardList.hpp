#ifndef BOARD_LIST_H
#define BOARD_LIST_H

#include "interface/CAEN_VX718.hpp"
#include "interface/CAEN_V1742.hpp"
#include "interface/CAEN_V1495PU.hpp"
#include "interface/CAEN_V1290.hpp"
#include "interface/CAEN_V814.hpp"
#include "interface/CAEN_V792.hpp"
#include "interface/CAEN_V785.hpp"
#include "interface/CAEN_V560.hpp"
#include "interface/CAEN_V513.hpp"
#include "interface/LECROY_1182.hpp"
#include "interface/MAROC_ROC.hpp"
#include "interface/CAEN_V265.hpp"
#include "interface/VFE_adapter.hpp"
#ifndef NO_FITXPIX
#include "interface/FITPIX.hpp"
#endif
#include "interface/TimeBoard.hpp"

#endif
